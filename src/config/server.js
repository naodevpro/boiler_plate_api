class Server {
  constructor({
    express,
    cors,
    routes,
    cookieParser,
    csrfMiddleware,
    handleError
  }) {
    this.app = express();
    this.initializeBodyParsing(express);
    this.initializeApplicationCors(cors);
    this.initializeMiddlewares({ cookieParser, csrfMiddleware });
    this.initializeApplicationRouter(routes);
    this.app.use((err, request, response, next) => {
      handleError(err, response);
    });
  }

  initializeBodyParsing(express) {
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(express.json());
  }

  initializeApplicationCors(cors) {
    this.app.use(
      cors({
        credentials: true,
        origin: ['http://localhost:3000']
      })
    );
  }

  initializeMiddlewares({ cookieParser, csrfMiddleware }) {
    this.app.use(cookieParser());
    this.app.get('/csrf', csrfMiddleware, (request, response) => {
      response.status(200).json(request.csrfToken());
    });
  }

  initializeApplicationRouter(routes) {
    this.app.use(routes);
  }

  listen(port) {
    this.app.listen(port || process.env.PORT, '0.0.0.0', () =>
      port
        ? console.log(
            ` 🚀 L'application à démarrer sur le port : ${
              port || process.env.PORT
            } 💥`
          )
        : console.log("🚀 L'application à démarrer sur Heroku 🟣💥")
    );
  }
}

export default Server;
