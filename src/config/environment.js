require('dotenv').config();

const config = {
  server_port: process.env.SERVER_PORT
};

module.exports = config;
